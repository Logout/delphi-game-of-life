unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls;

const gridWidth=25;
const gridHeight=12;
const cellSize = 20;
const arraySizeX = gridWidth - 1;
const arraySizeY = gridHeight - 1;

var grid: array [0..arraySizeX, 0..arraySizeY] of Boolean;
var nextRoundGrid: array [0..arraySizeX, 0..arraySizeY] of Boolean;
var round: Integer;

type
  TForm1 = class(TForm)
    Image1: TImage;
    StatusLabel: TLabel;
    ActionButton: TButton;
    Timer1: TTimer;
    constructor Create(AOwner: TComponent); override;
    procedure FormCreate(Sender: TObject);
    procedure Image1Click(Sender: TObject);
    procedure ActionButtonClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

constructor TForm1.Create(AOwner: TComponent);
var i,j: Integer;
begin
  inherited Create(AOwner);
  round := 0;
  for i:=0 to gridWidth do begin
    for j:=0 to gridHeight do begin
      grid[i][j] := False;
      nextRoundGrid[i][j] := False;
    end;
  end;
end;

procedure drawGrid(canvas: TCanvas);
var i: Integer;
begin
  with canvas do begin
    for i:=0 to gridWidth do begin
      MoveTo(i*cellSize, 0);
      LineTo(i*cellSize, gridHeight*cellSize);
    end;
    for i:=0 to gridHeight do begin
      MoveTo(0, i*cellSize);
      LineTo(gridWidth*cellSize, i*cellSize);
    end;
  end;
end;

procedure initialiseGrid;
var i,j: Integer;
begin
  for i:=0 to arraySizeX do
    for j:=0 to arraySizeY do
      nextRoundGrid[i][j] := grid[i][j];
end;

procedure copyGrid;
var i,j: Integer;
begin
  for i:=0 to arraySizeX do
    for j:=0 to arraySizeY do
      grid[i][j] := nextRoundGrid[i][j];
end;

procedure drawCells(canvas: TCanvas);
var i,j: Integer;
var currentState: Boolean;
var fillArea: TRect;
begin
  with canvas do begin
    for i:=0 to gridWidth do begin
      for j:=0 to gridHeight do begin
        currentState := grid[i][j];
        if currentState then
          Brush.Color := clBlack
        else
          Brush.Color := clWhite;
        fillArea.Left := i*cellSize+1;
        fillArea.Top := j*cellSize+1;
        fillArea.Right := (i+1)*cellSize;
        fillArea.Bottom := (j+1)*cellSize;
        FillRect(fillArea);
      end;
    end;
  end;
end;

function countNeighbouringLivingCells(x, y: Integer): Integer;
var i,j,currentNeighbourX,currentNeighbourY, livingNeighbouringCells: Integer;
begin
  livingNeighbouringCells := 0;
  for i:=-1 to 1 do begin
    for j:=-1 to 1 do begin
      if (i = 0) and (j = 0) then continue;
      currentNeighbourX := x+i;
      currentNeighbourY := y+j;
      if (currentNeighbourX < 0) or (currentNeighbourY < 0)
        or (currentNeighbourX > arraySizeX) or (currentNeighbourY > arraySizeY)
        then continue;
      if grid[currentNeighbourX][currentNeighbourY] then
        livingNeighbouringCells := livingNeighbouringCells + 1;
    end;
  end;
  countNeighbouringLivingCells := livingNeighbouringCells;
end;

procedure killCellLoneliness(x, y: Integer);
begin
  if countNeighbouringLivingCells(x,y) < 2 then nextRoundGrid[x][y] := False;
end;

procedure killCellOverpopulation(x, y: Integer);
begin
  if countNeighbouringLivingCells(x,y) > 3 then nextRoundGrid[x][y] := False;
end;

procedure reviveCell(x, y: Integer);
begin
  if countNeighbouringLivingCells(x,y) = 3 then nextRoundGrid[x][y] := True;
end;

procedure applyRules;
var i,j: Integer;
begin
  initialiseGrid;
  for i:=0 to gridWidth do begin
    for j:=0 to gridHeight do begin
      if grid[i][j] then begin
        killCellLoneliness(i, j);
        killCellOverpopulation(i, j);
      end
      else
        reviveCell(i, j);
    end;
  end;
  copyGrid;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
drawGrid(image1.Canvas);
end;

procedure TForm1.Image1Click(Sender: TObject);
var clickedPoint, clickedGridPoint, toggledCell: TPoint;
begin
clickedPoint := Mouse.CursorPos;
clickedGridPoint := ScreenToClient(clickedPoint);
clickedGridPoint.X := clickedGridPoint.X - Image1.Left;
clickedGridPoint.Y := clickedGridPoint.Y - Image1.Top;
toggledCell.X := clickedGridPoint.X div cellSize;
toggledCell.Y := clickedGridPoint.Y div cellSize;
grid[toggledCell.X, toggledCell.Y] := not grid[toggledCell.X, toggledCell.Y];
drawCells(Image1.Canvas);
end;


procedure iterateRound(canvas: TCanvas);
begin
applyRules();
drawCells(canvas);
round := round + 1;
end;

procedure TForm1.ActionButtonClick(Sender: TObject);
begin
Timer1.Enabled := not Timer1.Enabled;
if ActionButton.Caption = 'Start' then ActionButton.Caption := 'Stop'
else ActionButton.Caption := 'Start';
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
iterateRound(Image1.Canvas);
end;

end.

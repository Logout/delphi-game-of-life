object Form1: TForm1
  Left = 192
  Top = 107
  Width = 540
  Height = 380
  Caption = 'Game of Life'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Image1: TImage
    Left = 15
    Top = 50
    Width = 501
    Height = 241
    OnClick = Image1Click
  end
  object StatusLabel: TLabel
    Left = 176
    Top = 16
    Width = 177
    Height = 24
    Caption = 'Select your pattern'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
  object ActionButton: TButton
    Left = 224
    Top = 312
    Width = 75
    Height = 25
    Caption = 'Start'
    TabOrder = 0
    OnClick = ActionButtonClick
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 200
    OnTimer = Timer1Timer
    Left = 8
    Top = 16
  end
end
